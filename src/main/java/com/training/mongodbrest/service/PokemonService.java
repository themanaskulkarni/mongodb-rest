package com.training.mongodbrest.service;

import com.training.mongodbrest.model.Pokemon;
import com.training.mongodbrest.repository.PokemonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PokemonService implements PokemonServiceInterface {

    @Autowired
    private PokemonRepository pokemonRepository;

    public List<Pokemon> findAll() {
        return pokemonRepository.findAll();
    }

    public Optional<Pokemon> findById(String id) {
        return pokemonRepository.findById(id);
    }

    public Pokemon save(Pokemon pokemon) {
//        if (pokemon.getName()!=null && pokemon.getType()!=null)
            return pokemonRepository.save(pokemon);
//        else
//            return {"Error":"You"};
    }

    public void delete(String id){
        pokemonRepository.deleteById(id);
    }
}
