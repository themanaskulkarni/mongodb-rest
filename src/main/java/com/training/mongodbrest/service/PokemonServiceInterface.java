package com.training.mongodbrest.service;

import com.training.mongodbrest.model.Pokemon;

import java.util.List;
import java.util.Optional;

public interface PokemonServiceInterface {
    List<Pokemon> findAll();

    Optional<Pokemon> findById(String id);

    Pokemon save(Pokemon pokemon);

    void delete(String id);
}
