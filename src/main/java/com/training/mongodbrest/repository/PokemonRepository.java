package com.training.mongodbrest.repository;

import com.training.mongodbrest.model.Pokemon;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

@Component
public interface PokemonRepository extends MongoRepository<Pokemon, String> {

}
