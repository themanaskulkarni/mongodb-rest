package com.training.mongodbrest.controller;

import com.training.mongodbrest.model.Pokemon;
import com.training.mongodbrest.service.PokemonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.lang.invoke.WrongMethodTypeException;
import java.util.List;
import java.util.NoSuchElementException;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1/pokemon")
public class PokemonController {

    private static final Logger LOG = LoggerFactory.getLogger(PokemonController.class);

    @Autowired
    private PokemonService pokemonService;

    @GetMapping
    public List<Pokemon> findAll() {
        return pokemonService.findAll();
    }

    @GetMapping("{id}")
    public ResponseEntity<Pokemon> findById(@PathVariable String id) {
        LOG.debug("Inside findById!");
        try {
            return new ResponseEntity<Pokemon>(pokemonService.findById(id).get(), HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public ResponseEntity<Pokemon> save(@RequestBody Pokemon pokemon) {
//        return pokemonService.save(pokemon);

        try {
            return new ResponseEntity<Pokemon>(pokemonService.save(pokemon), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable String id) {
        if(pokemonService.findById(id).isEmpty()) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        pokemonService.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
